﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataInitializer
    {
        public DataInitializer(DbContext dbContext)
        {
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            dbContext.AddRange(FakeDataFactory.Roles);
            dbContext.AddRange(FakeDataFactory.Preferences);
            dbContext.AddRange(FakeDataFactory.Customers);
            dbContext.SaveChanges();
        }
    }
}