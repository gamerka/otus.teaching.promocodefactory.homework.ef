﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(20)]
        public string FirstName { get; set; }
        
        [MaxLength(20)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        public string Email { get; set; }

        public virtual List<CustomerPreference> Preferences { get; set; }

        public virtual List<PromoCode> PromoCodes { get; set; }
    }
}