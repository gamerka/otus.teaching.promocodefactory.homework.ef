﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Preference> preferenceRepository, 
            IRepository<Customer> customerRepository)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();
            var promocodesList = promocodes.Select(_ => new PromoCodeShortResponse
            {
                Id = _.Id,
                BeginDate = _.BeginDate.ToShortDateString(),
                EndDate = _.EndDate.ToShortDateString(),
                Code = _.Code,
                PartnerName = _.PartnerName,
                ServiceInfo = _.ServiceInfo
            }).ToList();

            return promocodesList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences?.SingleOrDefault(_ => _.Name == request.Preference);
            if (preference == null)
                BadRequest();

            var promocode = new PromoCode
            {
                Id = Guid.NewGuid(),
                Preference = preference,
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(7)
            };

            await _promocodeRepository.AddOneAsync(promocode);

            var customers = await _customerRepository.GetAllAsync();
            var customersWithPreference = customers.Where(c => c.Preferences.Any(p => p.Preference == preference));

            foreach (var customer in customersWithPreference)
            {
                customer.PromoCodes.Add(promocode);
                await _customerRepository.UpdateOneAsync(customer);
            }

            return Ok();
        }
    }
}