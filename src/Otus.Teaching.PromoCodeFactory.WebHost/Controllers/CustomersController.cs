﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository, 
            IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeRepository = promocodeRepository;
        }

        /// <summary>
        /// Получить всех Customers
        /// </summary>
        /// <returns>Список всех Customers</returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();

            return customersModelList;
        }
        
        /// <summary>
        /// Получить Customer по Id
        /// </summary>
        /// <param name="id">Id Customer</param>
        /// <returns>Customer</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var employeeModel = new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes.Select(_ => new PromoCodeShortResponse
                {
                    Id = _.Id,
                    BeginDate = _.BeginDate.ToShortDateString(),
                    EndDate = _.EndDate.ToShortDateString(),
                    Code = _.Code,
                    PartnerName = _.PartnerName,
                    ServiceInfo = _.ServiceInfo
                }).ToList(),
                Preferences = customer.Preferences.Select(_ => new PreferenceResponse
                {
                    Id = _.Preference.Id,
                    Name = _.Preference.Name
                }).ToList()
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Добавить нового Customer
        /// </summary>
        /// <param name="request">Тело Customer</param>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            await _customerRepository.AddOneAsync(new Customer
            {
                Id = Guid.NewGuid(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = request.PreferenceIds.Select(_ => new CustomerPreference{PreferenceId = _}).ToList()
            });

            return Ok();
        }
        
        /// <summary>
        /// Изменить Customer по Id
        /// </summary>
        /// <param name="id">Id Customer</param>
        /// <param name="request">Новое тело Customer</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var preferences = request.PreferenceIds?
                .Select(_ => _preferenceRepository.GetByIdAsync(_))
                .Select(_ => _.Result)
                .ToList();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Preferences.Clear();
            customer.Preferences = preferences?
                .Select(_ => new CustomerPreference { Preference = _ , Customer = customer})
                .ToList();

            await _customerRepository.UpdateOneAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить Customer по Id
        /// </summary>
        /// <param name="id">Id Customer</param>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var promocodes = customer.PromoCodes?
                .Select(_ => _promocodeRepository.GetByIdAsync(_.Id))
                .Select(_ => _.Result);
            if (promocodes != null)
                await Task.WhenAll(promocodes
                    .Select(_ => _promocodeRepository.DeleteOneAsync(_))
                    .ToList());

            await _customerRepository.DeleteOneAsync(customer);

            return Ok();
        }
    }
}